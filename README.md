# rhwf

rhwf - Revolution Pi Hardware Flasher

## Building

```
meson setup builddir
meson compile -C builddir
```

## Installing

```
meson install -C builddir --prefix '/'
```
